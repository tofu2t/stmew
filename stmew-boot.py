# -*- coding: utf-8 -*-

import os, json, datetime
from string import Template
from optparse import OptionParser

template = '''
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mew")
(require 'mew)

(defun stmew-main ()
    (mew-send "$to" nil "$subject")
    (mew-draft-prepare-attachments)
    $commands
    (mew-draft-send-message))

(stmew-main)
'''

# Args:
# - to: to
# - subject: subject
# - files: paths to files to attach
def output_elisp(to, subject, files, output='stmew.el'):
    if '$date' in subject:
        date = datetime.date.today().strftime('%Y%d%m')
        subject = Template(subject).substitute(date = date)
    command_lst = []
    for e in files:
        command_lst.append('(mew-attach-copy "%s" "%s")' % (os.path.abspath(e), e))
    with open(output, 'w') as f:
        f.write(Template(template).substitute(
            to = to, subject = subject, commands = '\n    '.join(command_lst)))

def read_stmewrc(filename):
    with open(filename) as f:
        return dict([(str(k), str(v)) for k, v in json.load(f).items()])

def main():
    config = {'to': None, 'subject': None, 'output': None, 'files': None}

    if os.path.exists('./.stmewrc'):
        config = read_stmewrc('./.stmewrc')

    user_cfg = os.path.expanduser('~/.stmewrc')
    if os.path.exists(os.path.expanduser(user_cfg)):
        config = read_stmewrc(user_cfg)

    usage = 'usage: %prog [options] args'
    op = OptionParser(usage)
    op.add_option('-t', '--to', dest = 'to')
    op.add_option('-s', '--subject', dest = 'subject')
    op.add_option('-o', '--output', dest = 'output')
    options, args = op.parse_args()
    if options.to:
        config['to'] = options.to
    if options.subject:
        config['subject'] = options.subject
    if options.output:
        config['output'] = options.output
    config['files'] = args

    output_elisp(**config)

if __name__ == '__main__':
    main()